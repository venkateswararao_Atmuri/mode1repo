package swap;

public class SwapFun {

	void swapNumbers(int a, int b) {
		System.out.println("A value before swap is: " + a);
		System.out.println("B value before swap is: " + b);
		a = a + b;
		b = a - b;
		a = a - b;
		System.out.println("A value after swap is: " + a);
		System.out.println("B values after swap is: " + b);

	}

}
