package exception;

import java.util.InputMismatchException;
import java.util.Scanner;

public class DividedByZero {

	public static void main(String[] args) {

		// System.out.println("connection established");
		try {
			Scanner scanner = new Scanner(System.in);
			System.out.println("Enter the two number");
			int num1 = scanner.nextInt();
			int num2 = scanner.nextInt();
			int result = num1 / num2;
			System.out.println(result);
			scanner.close();
			// System.out.println("concotion terminated");
		} catch (ArithmeticException e11) {
			System.out.println("java.lang.ArthamaticException:/by zero");
		} catch (InputMismatchException e11) {
			System.out.println("java.util.InputMismatchException");
		} catch (Exception e) {
			System.out.println("some issues has been occured");
		}
	}
}
