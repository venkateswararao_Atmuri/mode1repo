package myCalculator;

public class MyCalculator {
	public static long power(int n1, int n2) throws ArithmeticException,IllegalArgumentException,Exception {
		long result=1;
		if (n1 > 0 && n2 > 0) {
			
			for (; n2 != 0; --n2) {
				result *= n1;
			}
		}
		else if(n1==0 && n2==0) {
			throw new ArithmeticException("n and p should not be zero");
		}
		if(n1<0 && n2<0) {
			throw new IllegalArgumentException("n and p should not be negative ");
		}
		return result;
	}

}
