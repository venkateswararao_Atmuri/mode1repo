package reverse;

public class ReverseString {

	public static String wordReverse(String str) {
		{
			String rev = "";
			for (int i = str.length(); i > 0; --i) {
				rev = rev + (str.charAt(i - 1)) + "-";

			}

			return rev;
		}
	}

}
