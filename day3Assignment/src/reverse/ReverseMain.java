package reverse;

import java.util.Scanner;

public class ReverseMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		ReverseString reverse = new ReverseString();

		System.out.println("Enter the String:");
		String str = scanner.next();

		System.out.println("Reversed String is:" + reverse.wordReverse(str));

	}


	}
