package searchEle;

import java.util.Scanner;

public class SearchElement {
	public static void searchEle(int num) {
		Scanner sc = new Scanner(System.in);
		int a[] = new int[num];
		int i;
		int j;
		

		System.out.println("Enter Array Elements :");
		for (i = 0; i < a.length; i++) {
			System.out.println("Enter an Element");
			a[i] = sc.nextInt();
		}

		System.out.println("Array before Sorting");
		for (i = 0; i < a.length; i++) {
			System.out.println(a[i] + " ");
		}
		System.out.println();
		int temp;
        for(i= 0;i<=num-2;i++) {
		for (j = 0; j <= num - 2 - i; j++) {
			if (a[j] > a[j + 1]) {
				temp = a[j];
				a[j] = a[j + 1];
				a[j + 1] = temp;
			}
		}
	}
        System.out.println("Array after Sorting");
        for(i=0;i<num;i++){
        	System.out.println(a[i]+" ");
        }
        System.out.println();
        System.out.println("Enter a key to Search");
        int key=sc.nextInt();
        int low=0;
        int high=a.length-1;
        int mid;
        while(low<=high){
        	mid=(low+high)/2;
        	if(key==a[mid]){
        		System.out.println("Key is found at "+mid+" index");
        		System.exit(0);
        	}else if(key>a[mid]){
        		low=mid+1;
        		high=high;
        	}else {
        		low=low;
        		high=mid-1;
        	}
        }
        System.out.println("Key is not found");
        sc.close();
        
	}
}

        
