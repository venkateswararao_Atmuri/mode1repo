package stringLowerCase;

import java.util.Scanner;

public class StringCaseConverter {
	public void convertToLower() {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the String: ");
		String string = scanner.nextLine();
		char[] cs = string.toCharArray();
		int size = cs.length;
		for (int i = 0; i < size; i++) {
			if (cs[i] >= 'A' && cs[i] <= 'z') {
				cs[i] = (char) (cs[i] + 32);
			}
		}
		for (int i = 0; i < size; i++) {
			System.out.println(cs[i]);
		}
		scanner.close();
	}

}
