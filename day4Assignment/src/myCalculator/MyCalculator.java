package myCalculator;

import java.util.Scanner;

public class MyCalculator implements AdvancedArithemetic {
	int sumOfDivisors = 0;

	@Override
	public int divisor_sum(int n) {

		for (int i = 1; i <= n; i++) {
			if (n % i == 0) {
				sumOfDivisors += i;
			}
		}
		return sumOfDivisors;
	}

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the input");
		MyCalculator myCalculator = new MyCalculator();
		System.out.println(myCalculator.divisor_sum(sc.nextInt()));
		System.out.println("Enter the input");
		System.out.println(myCalculator.divisor_sum(sc.nextInt()));

		myCalculator = null;
		sc = null;
	}

}
