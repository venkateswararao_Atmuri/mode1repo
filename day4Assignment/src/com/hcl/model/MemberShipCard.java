package com.hcl.model;

public class MemberShipCard extends Card {
	
	private int rating;

	public MemberShipCard() {
		super();
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}
	
	public MemberShipCard(String holderName, String cardNumber, String expiryDate) {

		super(holderName, cardNumber, expiryDate);
	}

	public MemberShipCard(String holderName, String cardNumber, String expiryDate,int rating) {
		super(holderName, cardNumber, expiryDate);
		this.rating = rating;
	}
	
	

	
}
