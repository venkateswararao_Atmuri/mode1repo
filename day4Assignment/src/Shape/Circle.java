package Shape;

public class Circle extends Shape {
	private int radius;

	public Circle(String name, int radius) {
		this.radius = radius;
		this.calculateArea();
	}

	public float calculateArea() {
		return 3.14f * (this.radius) * (this.radius);
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

}
