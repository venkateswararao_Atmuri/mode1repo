package Shape;

public class Rectangle extends Shape {

	private int length;
	private int breadth;

	public Rectangle(String name, int length, int breadth) {
		this.length = length;
		this.breadth = breadth;
		this.calculateArea();
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getBreadth() {
		return breadth;
	}

	public void setBreadth(int breadth) {
		this.breadth = breadth;
	}

	@Override
	public float calculateArea() {

		return this.length * this.breadth;
	}

}
