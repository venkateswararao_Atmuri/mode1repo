package Shape;

public abstract class Shape {
	private String name;

	Shape() {

	}

	abstract float calculateArea();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
