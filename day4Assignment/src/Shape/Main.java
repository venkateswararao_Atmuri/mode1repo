package Shape;

public class Main {

	public static void main(String[] args) {

		Rectangle rectangle = new Rectangle("rectangle", 3, 4);
		Circle circle = new Circle("circle", 5);
		Square square = new Square("square", 7);
		System.out.println("Area of rectangle is: " + rectangle.calculateArea());
		System.out.println("Area of Circle is: " + circle.calculateArea());
		System.out.println("Area of square is: " + square.calculateArea());

	}

}
