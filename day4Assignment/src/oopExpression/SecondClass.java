package oopExpression;

public class SecondClass {
	double b = 123.45;

	public SecondClass() {
		System.out.println("------in the constructor if class B:");
		System.out.println("b=" + b);
		b = 3.14519;
		System.out.println("b=" + b);

	}

	public void setSecondClass(double value) {
		b = value;
	}

	public double getSecondClass() {
		return b;
	}

}
