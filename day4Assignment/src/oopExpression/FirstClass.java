package oopExpression;

public class FirstClass {
	int a = 100;

	public FirstClass() {
		System.out.println("in the constructor of class Firstclass");
		System.out.println("a=" + a);
		a = 333;
		System.out.println("a=" + a);
	}

	public void setFirstClass(int value) {
		a = value;
	}

	public int getFirstClass() {
		return a;
	}

}
