package vowelCount;

import java.util.Scanner;

public class CountVowels {
	public int count() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enetr the String: ");
		String str = sc.nextLine();
		str = str.toLowerCase();
		char[] c = str.toCharArray();
		int size = c.length;
		int count = 0;
		for (int i = 0; i < size; i++) {
			if (c[i] >= 'a' && c[i] <= 'z') {
				if (c[i] == 'a' || c[i] == 'e' || c[i] == 'i' || c[i] == 'o' || c[i] == 'u') {
					count++;
				}
			}
		}
		sc.close();

		return count;

	}

}
