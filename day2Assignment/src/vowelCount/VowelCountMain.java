package vowelCount;

public class VowelCountMain {

	public static void main(String[] args) {

		CountVowels countVowels = new CountVowels();
		int vowelCount = countVowels.count();
		System.out.println("The Vowels In the String are: " + vowelCount);

	}

}
