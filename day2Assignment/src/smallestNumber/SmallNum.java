package smallestNumber;

import java.util.Scanner;

public class SmallNum {

	public int findSmallestNumber() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Array Length: ");
		int length = sc.nextInt();
		int num[] = new int[length];
		System.out.println("Enter Array Size: ");
		for (int i = 0; i < num.length; i++) {
			System.out.println("Enter an Element:");
			num[i] = sc.nextInt();

		}
		int temp;
		for (int i = 0; i < num.length; i++) {
			for (int j = i + 1; j < num.length; j++) {
				if (num[i] > num[j]) {
					temp = num[i];
					num[i] = num[j];
					num[j] = temp;

				}

			}

		}
		sc.close();
		return num[0];

	}
}
