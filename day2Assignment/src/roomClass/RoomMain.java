package roomClass;

public class RoomMain {

	public static void main(String[] args) {

		display();

	}

	public static void display() {
		Room room1 = new Room(12, "square", "20feets", "LG ACmachine");
		Room room2 = new Room(13, "Circle", "30feets", "Swift Machine");
		Room[] rooms = new Room[2];

		room1.setData(rooms);
		room2.setData(rooms);

		System.out.println("Room Number is :" + room1.getRoomNo());
		System.out.println("Room type is :" + room1.getRoomType());
		System.out.println("Room Area is :" + room1.getRoomArea());
		System.out.println("Room ACMachine is :" + room1.getAcMachine());

		System.out.println("Room Number is :" + room2.getRoomNo());
		System.out.println("Room type is :" + room2.getRoomType());
		System.out.println("Room Area is :" + room2.getRoomArea());
		System.out.println("Room ACMachine is :" + room2.getAcMachine());

	}

}
