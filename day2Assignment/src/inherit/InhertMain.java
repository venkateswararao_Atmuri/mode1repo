package inherit;


class A {
	  
	public void display(){
		
		System.out.println("Class A");
	}
	          
}
 class B extends A {
	 public void display2(){
		 System.out.println("Class B");
	 }
}


public class InhertMain {

	public static void main(String[] args) {
		B b=new B();
		b.display();
		b.display2();
		

	}

}
