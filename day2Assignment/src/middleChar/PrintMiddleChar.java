package middleChar;

public class PrintMiddleChar {

	public static void PrintMiddleC(String str) {
		int length = str.length();
		int middle;
		if (length % 2 != 0) {
			middle = length / 2;
		} else {
			middle = (length + 1) / 2;
		}
		System.out.println("The Middle Character in the String :" + str.charAt(middle));
	}
}
