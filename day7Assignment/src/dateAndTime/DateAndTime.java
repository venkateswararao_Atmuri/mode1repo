package dateAndTime;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateAndTime {

	public static void main(String[] args) {

		Date date = new Date(); // deprecated==old version
		System.out.println(date);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/mm/yyyy  hh:mm:ss");// patern
		System.out.println(simpleDateFormat.format(date));
		// SimpleDateFormat simpleDateFormat1=new SimpleDateFormat(" hh:mm:ss");
		// Date date2=simpleDateFormat1.parse("23:00:07");
		// System.out.println(date2);
		Calendar calendar = Calendar.getInstance();
		System.out.println("The current time is : " + calendar.getTime());
	}

}
